﻿using UnityEngine;
using System.Collections;

public class WinRectController : MonoBehaviour
{

    public int headsInTrigger = 0;
    private Rigidbody2D lastHead;

    void Awake()
    {
    }

    void Start()
    {
    }

    void Update()
    {

        if (headsInTrigger > 0 && Time.frameCount%10 == 0)
        {
            var vel = lastHead.velocity;
            if (vel.SqrMagnitude() < 0.5*0.5)
            {
                UiLogic.instance.ShowScreenWin();
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        headsInTrigger++;
        lastHead = other.attachedRigidbody;
    }

    void OnTriggerLeave2D(Collider2D other)
    {
        headsInTrigger--;
    }
}