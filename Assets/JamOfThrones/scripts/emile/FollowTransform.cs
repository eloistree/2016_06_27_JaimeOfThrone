﻿using System;
using UnityEngine;
using System.Collections;

public class FollowTransform : MonoBehaviour
{
    public Transform target;
    public RectOffset border; // = new RectOffset(50, 50, 50, 50);
    private Camera cam;
    private Vector3 initPos;


    void Awake()
    {
        cam = GetComponent<Camera>();
        initPos = transform.position;
    }

    void Update()
    {
        if (target == null)
            return;

        float d = 5;
        var leftTop = cam.ScreenToWorldPoint(new Vector3(border.left, border.top, d));
        var rightBottom = cam.ScreenToWorldPoint(new Vector3(
            cam.pixelWidth - border.right, cam.pixelHeight - border.bottom, d));

        var rect2d = new Rect(leftTop.x, leftTop.y, 0, 0);
        rect2d.xMax = rightBottom.x;
        rect2d.yMax = rightBottom.y;

        Vector3 pos = target.transform.position;
        pos.x = Mathf.Min(rect2d.xMax, Mathf.Max(rect2d.xMin, pos.x));
        pos.y = Mathf.Min(rect2d.yMax, Mathf.Max(rect2d.yMin, pos.y));
        var relChange = target.transform.position - pos;
        transform.position += relChange;

    }

    void OnDrawGizmos()
    {
        cam = GetComponent<Camera>();
        float d = 5;
        var leftTop = cam.ScreenToWorldPoint(new Vector3(border.left, border.top, d));
        var rightBottom = cam.ScreenToWorldPoint(new Vector3(
            cam.pixelWidth - border.right, cam.pixelHeight - border.bottom, d));
        var rightTop = cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth - border.right, border.top, d));
        var leftBottom = cam.ScreenToWorldPoint(new Vector3(
            border.left, cam.pixelHeight - border.bottom, d));

        Gizmos.color = Color.red;
        Gizmos.DrawLine(leftTop, rightTop);
        Gizmos.DrawLine(rightTop, rightBottom);
        Gizmos.DrawLine(rightBottom, leftBottom);
        Gizmos.DrawLine(leftBottom, leftTop);
    }

    public void ResetPosAndTarget()
    {
        target = null;
        transform.position = initPos;
    }
}
