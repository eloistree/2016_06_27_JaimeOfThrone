﻿using UnityEngine;
using System.Collections;

public class BloodController : MonoBehaviour
{
    private Animator an;
    void Awake()
    {
        an = GetComponent<Animator>();
        an.speed = 2;
    }

    void OnSpawned()
    {
        StartCoroutine(despawnAfterAnimationTime());
    }

    IEnumerator despawnAfterAnimationTime()
    {
        float delay = an.GetCurrentAnimatorStateInfo(0).length / an.speed;
        yield return new WaitForSeconds(delay);
        SimplePool.instance.Despawn(gameObject);
    }
    void Update()
    {
    }
}
