﻿using UnityEngine;
using System.Collections;
using System.Security.AccessControl;

public class CastleController : MonoBehaviour
{
    public RectOffset randRect = new RectOffset();
    public Vector3 oPos; // original position

    void Awake()
    {
        oPos = transform.position;
    }

    void Start()
    {
        var pos = oPos;
        pos.x = Random.Range(pos.x + randRect.left, pos.x + randRect.right);
        pos.y = Random.Range(pos.y + randRect.top, pos.y + randRect.bottom);
        transform.position = pos;
    }

    void Update()
    {
    }

    void OnDrawGizmos()
    {
        Awake();
        Gizmos.color = new Color(1,0,0,0.4f);
        var pos = transform.position;
        var s = new Vector3();
        s.x= oPos.x + randRect.left - (oPos.x + randRect.right);
        s.y = oPos.y + randRect.top - (oPos.y + randRect.bottom);
        s.z = 1;
        var c = new Vector3();
        c.x = oPos.x + randRect.left + oPos.x + randRect.right;
        c.y = oPos.y + randRect.top + oPos.y + randRect.bottom;
        c.x *= 0.5f;
        c.y *= 0.5f;


        Gizmos.DrawCube(c, s);
    }

}
