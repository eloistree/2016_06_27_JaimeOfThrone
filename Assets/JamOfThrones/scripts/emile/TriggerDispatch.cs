﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class TriggerDispatch : MonoBehaviour
{
    public UnityEvent unityEvent;

    void Awake()
    {
    }

    void Update()
    {
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        unityEvent.Invoke();
    }
}
