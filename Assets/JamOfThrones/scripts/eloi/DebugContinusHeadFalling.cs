﻿using UnityEngine;
using System.Collections;

public class DebugContinusHeadFalling : MonoBehaviour {

    public Transform _popZone;
    public GameObject _headPrefab;
    public ThrowHeadForce _throwHeadScript;

    public void Start()
    {
        InvokeRepeating("ThrowHead", 0, 1);

    }

    public void ThrowHead() {

        GameObject headCreated = GameObject.Instantiate( _headPrefab , _popZone.position , Quaternion.identity) as GameObject;
        Rigidbody2D headRig = headCreated.GetComponent<Rigidbody2D>();
        float randomRotation = Random.Range(0f, 90f);
        float randomForce = Random.Range(0f, 90f);
        _throwHeadScript.AddForceToHead(headRig, randomRotation, randomForce);
    }
}
