﻿using UnityEngine;
using System.Collections;

public class ThrowHeadForce : MonoBehaviour {

    public float _minforce = 1f;
    public float _maxForce = 1f;
    public float _maxAngle = 3f;
    public float _min2DTorque = -2f;
    public float _max2DTorque = -10f;



    public void AddForceToHead(Rigidbody2D target, float rotationInDegree, float powerPourcent) {

        Vector2 forceToApply;
        ForceMode2D forceType = ForceMode2D.Impulse;


        rotationInDegree = Mathf.Clamp(rotationInDegree, 0f, 90f);
        forceToApply.x = Mathf.Cos(Mathf.Deg2Rad*rotationInDegree);
        forceToApply.y= Mathf.Sin(Mathf.Deg2Rad * rotationInDegree);

        forceToApply *= _minforce + _maxForce * powerPourcent;

        target.AddForce(forceToApply , forceType);
        target.AddTorque( Random.Range(_min2DTorque,_max2DTorque) );
    }
}
