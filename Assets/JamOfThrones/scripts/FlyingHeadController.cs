﻿using UnityEngine;
using System.Collections;

public class FlyingHeadController : MonoBehaviour
{
    public Vector2 debugInitVel = new Vector2(10, 5);
    public Rigidbody2D rb;
    public Transform futurePos;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        rb.AddForce(this.debugInitVel);
    }

    void Update()
    {
        var vel3 = new Vector3(rb.velocity.x, rb.velocity.y, 0);
        futurePos.position += ((transform.position + vel3 * 0.4f) - futurePos.position)*0.2f;
    }
}
