﻿using UnityEngine;
using System.Collections;
using System;

public class LaunchHeadOnScreenRelease : MonoBehaviour {

    public ScreenLaunchFromPoint _screenReleasePointScript;

    public float _timeBeforeLaunchHead = 0.5f;
    public ThrowHeadForce _headThrowScript;
    public GameObject _headLoadedToLaunch;


    private float _lastAngleChoose;
    private float _lastForceChoose;
	// Use this for initialization
	void Start () {
        _screenReleasePointScript.onScreenRelease += LaunchHeadIfPossible;
    }


    void Destroy()
    {
        _screenReleasePointScript.onScreenRelease -= LaunchHeadIfPossible;

    }

    private void LaunchHeadIfPossible(float angleInDegree, float forcePourcent)
    {
        _lastAngleChoose = angleInDegree;
        _lastForceChoose = forcePourcent;
        if (_headLoadedToLaunch)
        {
            Invoke( "LaunchHead", _timeBeforeLaunchHead);
        }
    }
    
    public void LaunchHead() {
        if (_headLoadedToLaunch == null) return;
        Rigidbody2D headRigid = _headLoadedToLaunch.GetComponent<Rigidbody2D>();
        _headThrowScript.AddForceToHead(headRigid, _lastAngleChoose, _lastForceChoose);
        _headLoadedToLaunch = null;
    }
}
