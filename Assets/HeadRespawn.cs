﻿using UnityEngine;
using System.Collections;
using System;

public class HeadRespawn : MonoBehaviour {


    public GameObject _headPrefab;
    public Transform _headPopPoint;

    public float _timeToRespawn;
    public FollowTransform _cameraFollower;
    public LaunchHeadOnScreenRelease _headLauncher;
    public ScreenLaunchFromPoint _screenLaunch;

    public float _popCountdown;

    public void Start() {
        _screenLaunch.onScreenRelease += MakeRepopHead;
        MakePopHead();
    }

    public void Update()
    {

        if (_popCountdown > 0f)
        {

            _popCountdown -= Time.deltaTime;
            if (_popCountdown <= 0f)
            {
                MakePopHead();
            }
        }

    }

    private void MakeRepopHead(float angleInDegree=0, float forcePourcent=0)
    {
        // Invoke("MakePopHead", _timeToRespawn);

        if(_popCountdown <=0f )
        _popCountdown = _timeToRespawn;



    }

    public void MakePopHead() {
        if (_headLauncher._headLoadedToLaunch != null) return;
        GameObject headCreated = GameObject.Instantiate(_headPrefab, _headPopPoint.position, Quaternion.identity) as GameObject;
        _headLauncher._headLoadedToLaunch = headCreated;
        _cameraFollower.ResetPosAndTarget();
        _cameraFollower.target = headCreated.transform;

    }

}
